package com.binary_studio.fleet_commander.core.subsystems;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powergridRequirments;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		var instance = new AttackSubsystemImpl();
		instance.name = Optional.of(name).filter(s -> !s.isBlank())
				.orElseThrow(() -> new IllegalArgumentException("Name should be not null and not empty"));
		instance.powergridRequirments = powergridRequirments;
		instance.capacitorConsumption = capacitorConsumption;
		instance.optimalSpeed = optimalSpeed;
		instance.optimalSize = optimalSize;
		instance.baseDamage = baseDamage;
		return instance;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		var sizeReductionModifier = 0.0;
		var speedReductionModifier = 0.0;

		if (target.getSize().value() >= this.optimalSize.value()) {
			sizeReductionModifier = 1.0;
		}
		else {
			sizeReductionModifier = (double) (target.getSize().value()) / this.optimalSize.value();
		}

		if (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) {
			speedReductionModifier = 1.0;
		}
		else {
			speedReductionModifier = (double) (this.optimalSpeed.value()) / (2 * target.getCurrentSpeed().value());
		}
		var damage = this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier);
		if (damage > 0 && damage < 1) {
			return PositiveInteger.of(1);
		}
		return PositiveInteger.of((int) Math.ceil(damage));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
