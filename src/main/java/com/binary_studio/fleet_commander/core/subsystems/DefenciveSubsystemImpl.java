package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger powergridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		DefenciveSubsystemImpl instance = new DefenciveSubsystemImpl();
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		instance.name = name;
		instance.powergridConsumption = powergridConsumption;
		instance.capacitorConsumption = capacitorConsumption;
		instance.impactReductionPercent = impactReductionPercent;
		instance.shieldRegeneration = shieldRegeneration;
		instance.hullRegeneration = hullRegeneration;

		return instance;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		Double computingDamage = Double.valueOf(incomingDamage.damage.value());
		var reductionPercent = this.impactReductionPercent.value();
		if (reductionPercent > 95) {
			computingDamage = computingDamage - (computingDamage * 95 / 100.0);
		}
		else {
			computingDamage = computingDamage - (computingDamage * reductionPercent / 100.0);
		}
		if (computingDamage > 0 && computingDamage < 1) {
			computingDamage = 1.0;
		}
		var resultDamage = PositiveInteger.of((int) Math.ceil(computingDamage));
		return new AttackAction(resultDamage, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
