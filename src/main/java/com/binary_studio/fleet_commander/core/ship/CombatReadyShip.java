package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private final DockedShip ship;

	private PositiveInteger hullHPCurrentAmount;

	private PositiveInteger shieldHPCurrentAmount;

	private PositiveInteger capacitorCurrentAmount;

	public CombatReadyShip(DockedShip ship) {
		this.ship = ship;
		this.hullHPCurrentAmount = ship.getHullHP();
		this.shieldHPCurrentAmount = ship.getShieldHP();
		this.capacitorCurrentAmount = ship.getCapacitorAmount();
	}

	@Override
	public void endTurn() {
		var chargeAmount = this.capacitorCurrentAmount.value() + this.ship.getCapacitorRechargeRate().value();
		if (chargeAmount > this.ship.getCapacitorAmount().value()) {
			this.capacitorCurrentAmount = PositiveInteger.of(this.ship.getCapacitorAmount().value());
		}
		else {
			this.capacitorCurrentAmount = PositiveInteger.of(chargeAmount);
		}

	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		int energyConsumption = this.capacitorCurrentAmount.value()
				- this.ship.getAttackSubsystem().getCapacitorConsumption().value();
		if (energyConsumption < 0) {
			return Optional.empty();
		}

		this.capacitorCurrentAmount = PositiveInteger.of(energyConsumption);

		return Optional.of(new AttackAction(this.ship.getAttackSubsystem().attack(target), this, target,
				this.ship.getAttackSubsystem()));

	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedDamage = this.ship.getDefenciveSubsystem().reduceDamage(attack);
		var receivedDamage = this.shieldHPCurrentAmount.value() - reducedDamage.damage.value();
		if (receivedDamage < 0) {
			this.shieldHPCurrentAmount = PositiveInteger.of(0);
			if (this.hullHPCurrentAmount.value() - Math.abs(receivedDamage) <= 0) {
				return new AttackResult.Destroyed();
			}
			this.hullHPCurrentAmount = PositiveInteger.of(this.hullHPCurrentAmount.value() - Math.abs(receivedDamage));
		}
		else {
			this.shieldHPCurrentAmount = PositiveInteger.of(receivedDamage);
		}

		return new AttackResult.DamageRecived(reducedDamage.weapon, reducedDamage.damage, reducedDamage.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {

		var capOfHullHP = this.ship.getHullHP().value();
		var capOfShieldHP = this.ship.getShieldHP().value();
		var powerConsumed = this.capacitorCurrentAmount.value()
				- this.ship.getDefenciveSubsystem().getCapacitorConsumption().value();
		if (powerConsumed < 0) {
			return Optional.empty();
		}
		this.capacitorCurrentAmount = PositiveInteger.of(powerConsumed);

		Optional<RegenerateAction> regenerate = Optional.of(this.ship.getDefenciveSubsystem().regenerate());
		var hullHPDelta = capOfHullHP - this.hullHPCurrentAmount.value();
		var shieldHPDelta = capOfShieldHP - this.shieldHPCurrentAmount.value();
		var regeneratedHullHPAmount = 0;
		var regeneratedShieldHPAmount = 0;

		if (hullHPDelta == 0 && shieldHPDelta == 0) {
			return Optional
					.of(new RegenerateAction(PositiveInteger.of(shieldHPDelta), PositiveInteger.of(hullHPDelta)));
		}

		var hullRegeneratedPoints = regenerate.get().hullHPRegenerated.value();
		var shieldRegeneratedPoints = regenerate.get().shieldHPRegenerated.value();

		if (this.hullHPCurrentAmount.value() + hullRegeneratedPoints > capOfHullHP) {
			this.hullHPCurrentAmount = PositiveInteger.of(capOfHullHP);
			regeneratedHullHPAmount = hullHPDelta;
		}
		else {
			this.hullHPCurrentAmount = PositiveInteger.of(this.hullHPCurrentAmount.value() + hullRegeneratedPoints);
			regeneratedHullHPAmount = hullRegeneratedPoints;
		}

		if (this.shieldHPCurrentAmount.value() + shieldRegeneratedPoints > capOfShieldHP) {
			this.shieldHPCurrentAmount = PositiveInteger.of(capOfShieldHP);
			regeneratedShieldHPAmount = shieldHPDelta;
		}
		else {
			this.shieldHPCurrentAmount = PositiveInteger
					.of(this.shieldHPCurrentAmount.value() + shieldRegeneratedPoints);
			regeneratedShieldHPAmount = shieldRegeneratedPoints;
		}

		return Optional.of(new RegenerateAction(PositiveInteger.of(regeneratedShieldHPAmount),
				PositiveInteger.of(regeneratedHullHPAmount)));
	}

}
