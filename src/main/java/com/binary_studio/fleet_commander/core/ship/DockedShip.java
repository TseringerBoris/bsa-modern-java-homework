package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger capacitorAmount;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private DockedShip(PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitorAmount) {
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitorAmount = capacitorAmount;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		var instance = new DockedShip(shieldHP, hullHP, capacitorAmount);
		instance.name = name;
		instance.powergridOutput = powergridOutput;
		instance.capacitorRechargeRate = capacitorRechargeRate;
		instance.speed = speed;
		instance.size = size;

		return instance;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getPowergridOutput() {
		return this.powergridOutput;
	}

	public PositiveInteger getCapacitorAmount() {
		return this.capacitorAmount;
	}

	public PositiveInteger getCapacitorRechargeRate() {
		return this.capacitorRechargeRate;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {

		if (this.defenciveSubsystem != null) {
			var missingPower = this.powergridOutput.value() - (this.defenciveSubsystem.getPowerGridConsumption().value()
					+ subsystem.getPowerGridConsumption().value());
			if (missingPower < 0) {
				throw new InsufficientPowergridException(missingPower);
			}
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (this.attackSubsystem != null) {
			var missingPower = this.powergridOutput.value() - (this.attackSubsystem.getPowerGridConsumption().value()
					+ subsystem.getPowerGridConsumption().value());
			if (missingPower < 0) {
				throw new InsufficientPowergridException(missingPower);
			}
		}
		this.defenciveSubsystem = subsystem;

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem == null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}

		return new CombatReadyShip(this);
	}

}
