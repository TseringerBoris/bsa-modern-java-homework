package com.binary_studio.tree_max_depth;

import java.util.ArrayDeque;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {

	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		var maxDepth = 0;
		if (rootDepartment == null) {
			return maxDepth;
		}

		if (rootDepartment.subDepartments.isEmpty()) {
			return ++maxDepth;
		}

		var queue = new ArrayDeque<Department>();
		queue.add(rootDepartment);

		while (!queue.isEmpty()) {
			maxDepth++;
			for (var currentDepartment : queue) { // traversing current scope
				queue.remove();
				if (currentDepartment.subDepartments != null) { // Is the current node a
																// leaf?
					for (var subDepartment : currentDepartment.subDepartments) {
						if (subDepartment != null) { // Is sub-department has its own
														// sub-departments?
							queue.add(subDepartment);
						}
					}
				}
			}
		}

		return maxDepth;
	}

}
